import tensorflow as tf
import random
class Params:
    def __init__(self):
        self.embed_size = 75
        self.batch_size = 20000
        self.epochs = 500
        self.learning_rate = 0.01

        self.top_k = 20

        self.ent_top_k = [1, 5, 10, 50]
        self.nums_threads = 20

        self.lambda_1 = 0.01
        self.lambda_2 = 2.0
        self.lambda_3 = 0.7
        self.lambda_4 = 0.95
        self.mu_1 = 0.2

        self.epsilon = 0.9
        self.nums_neg = 10

        self.heuristic = True
        self.lang=None
        self.init_w2v=False
        if self.init_w2v:self.embed_size=300
        self.qvec=True 
        self.likelihood=False
        self.mqvec=False
        self.qvec2=False
        self.minibatch=False
        self.dqvec=False
        self.kernelType="id"
        self.kernel=lambda x : x#tf.square(x)
        self.predicted_ref=False
        self.qvec_ref=False
        self.alignp=False
        self.output=""
        self.matcher=""#JaroWinkler"
        self.corrupted=False
        self.rate=30
        self.bestQVEC=False
        self.bestBootEA=True

    def print(self):
        print("Parameters used in this running are as follows:")
        if self.likelihood:self.output+="lh"
        if self.init_w2v:self.output+=".w2v"
        if self.qvec:self.output+=".qvec"
        if self.qvec2:self.output+=".qvec2"
        if self.mqvec:self.output+=".mqvec"
        if self.dqvec:self.output+=".dqvec"
        if self.minibatch:self.output+=".mb"
        for item in self.__dict__.items():
            print("%s: %s" % item)
        print()


P = Params()
P.print()
