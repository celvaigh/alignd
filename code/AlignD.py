#Copyright (c) 2018 Websoft@NJU
import sys
import time

from train_funcs import get_model, generate_related_mat, train_tris_k_epo, train_alignment_1epo
from train_bp import bootstrapping, likelihood,likelihood2,likelihood3
from model import P
import gc

import utils as ut


def train(folder):
    ori_triples1, ori_triples2, triples1, triples2, model = get_model(folder)
    hits1 = None

    labeled_align = set()
    ents1, ents2 = None, None

    # related_mat = generate_related_mat(folder, triples1, triples2, model.ref_ent1, model.ref_ent2)
    related_mat = None
    pairs=[]
    if P.bestQVEC or P.bestBootEA:
        if P.bestBootEA:pairs=ut.get_best_resf(folder,P.matcher+"bestBootEA")
        else:pairs=ut.get_best_resf(folder,P.matcher+"bestQVEC")
        labeled_align = set(pairs)
    #pairs=ut.get_best_resf(folder,"bestRDGCN")
    
    if P.qvec:output="qvec"
    else:output="bootea"
    #if P.init_w2v:output+=".w2v"
    #output+=".rdgcn"

    if P.epsilon > 0:
        trunc_ent_num = int(len(ori_triples1.ent_list) * (1 - P.epsilon))
        assert trunc_ent_num > 0
        print("trunc ent num:", trunc_ent_num)
    else:
        trunc_ent_num = 0
        assert not trunc_ent_num > 0
    if "15" in folder or "anatomy-dataset" in folder:
        for t in range(1, P.epochs//10 + 1):
            print("iteration ", t)
            gc.collect()
            train_tris_k_epo(model, triples1, triples2, 5, trunc_ent_num, None, None)
            train_alignment_1epo(model, triples1, triples2, ents1, ents2, 1)
            train_tris_k_epo(model, triples1, triples2, 5, trunc_ent_num, None, None)
            labeled_align, ents1, ents2 = bootstrapping(model, related_mat, labeled_align)
            train_alignment_1epo(model, triples1, triples2, ents1, ents2, 1)
            hits1 = model.test(selected_pairs=labeled_align)
            if P.qvec or P.likelihood:
                if P.qvec and not P.likelihood:likelihood2(model, labeled_align,pairs)
                elif P.qvec and P.likelihood:likelihood3(model, labeled_align)
                else:likelihood(model, labeled_align)
                model.test(selected_pairs=labeled_align)
            # if t%10==0:
            #     ut.pair2file(folder + output + str(t), hits1)
            #     model.save(folder, output + str(t))
        if P.qvec:output=P.matcher+"bestQVEC"
        else:output=P.matcher+"bestBootEA"
        #output+=".RDCGCN"
        # ut.pair2file(folder +output, hits1)
        # model.save(folder, output)
    else:
        for t in range(1, P.epochs//10 + 1):
            print("iteration ", t)
            gc.collect()
            train_tris_k_epo(model, triples1, triples2, 5, trunc_ent_num, None, None)
            train_alignment_1epo(model, triples1, triples2, ents1, ents2, 1)
            train_tris_k_epo(model, triples1, triples2, 5, trunc_ent_num, None, None)
            labeled_align, ents1, ents2 = bootstrapping(model, related_mat, labeled_align)
            train_alignment_1epo(model, triples1, triples2, ents1, ents2, 1)
            hits1 = model.test(selected_pairs=labeled_align)
            if P.qvec :
                likelihood2(model, labeled_align,pairs)
                #else:likelihood(model, labeled_align)
                model.test(selected_pairs=labeled_align)
            # if t%10==0:
            #     ut.pair2file(folder + output + str(t), hits1)
            #     model.save(folder, output + str(t))
        if P.qvec:output=P.matcher+"bestQVEC"
        else:output=P.matcher+"bestBootEA"
        #output+=".RDCGCN"
        ut.pair2file(folder +output, hits1)
        model.save(folder, output)


if __name__ == '__main__':
    t = time.time()
    if len(sys.argv) == 2:
        folder = sys.argv[1]
    else:
        folder = '.'
    if "fr_" in folder:P.lang="fr"
    elif "zh_" in folder:P.lang="zh"
    elif "ja_" in folder:P.lang="ja"
    train(folder)
    print("total time = {:.3f} s".format(time.time() - t))
