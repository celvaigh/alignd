package StringMatchers;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Locale;

import org.allenai.word2vec.Searcher;
import org.allenai.word2vec.Word2VecModel;
import org.allenai.word2vec.Searcher.UnknownWordException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.Syntax;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RIOT;
import org.apache.jena.sparql.function.FunctionRegistry;

public class StringMatcher {

	public static Searcher wordVectors=null;
	public static String matcherName="";
	public static String findCandidates(String label,Model model, String functionUri,String matchcond) {
		//System.out.println("label:"+label);
		String queryString="SELECT ?entity "
				+ " WHERE { "
				+ "?entity ?rel ?o "
				+ "FILTER(<"+functionUri +">(?entity, \"" + label + "\") "+matchcond+") }";
		try {
			Query query = QueryFactory.create("PREFIX owl:<http://www.w3.org/2002/07/owl#> "+queryString,Syntax.syntaxSPARQL_11) ;
			QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
			ResultSet results = qexec.execSelect();
			Resource entity = null,tentity = null;
			double score=0,tscore=0;
			while ( results.hasNext() ) {
	            QuerySolution qs = results.next();
	            entity = qs.getResource( "entity" ).asResource();
	            if(matcherName.equals("Fuzzy"))tscore=StringUtils.getFuzzyDistance(entity.toString(),label,Locale.ENGLISH); 
	    		else if(matcherName.equals("JaroWinkler"))tscore=StringUtils.getJaroWinklerDistance(entity.toString(),label); 
	    		else if(matcherName.equals("DSC"))tscore=DSCFilter.DSC(entity.toString(),label); 
	    		else if(matcherName.equals("W2V")) {
	    			String[] values1 = label.split("/");
	    			String[] values2 = entity.toString().split("/");
	    			try {
	    				tscore= wordVectors.cosineDistance(values2[values2.length-1].toLowerCase(),Arrays.asList(values1).get(values1.length-1).toLowerCase());
	    			} catch (UnknownWordException e) {
	    				// TODO Bloc catch généré automatiquement
	    				tscore=0;
	    			}
	    		}
	            if(tscore>score) {
	            	score=tscore; tentity=entity;
	            }
	           if(functionUri.contains("SubString") || functionUri.contains("Levenshtein"))return entity.toString();
			}
			return tentity.toString();
		}
		catch(Exception e) {
			//System.out.println(e);
			return null;
		}
	}

	public static ArrayList<String> getEntities(String kbname){
		ArrayList<String> list = new ArrayList<String>();
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(kbname));
			String line;
			try {
				while ((line = br.readLine()) != null) {
					String[] values = line.split("\t");
					list.add(Arrays.asList(values).get(values.length-1));
				}
			} catch (IOException e) {
				// TODO Bloc catch généré automatiquement
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Bloc catch généré automatiquement
			e.printStackTrace();
		}
		return list;
	}
	public static Dictionary<String,String> makeDic(String filename){
		Dictionary<String,String> dic = new Hashtable<String,String>();
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(filename));
			String line;
			try {
				while ((line = br.readLine()) != null) {
					String[] values = line.split("\t");
					dic.put(Arrays.asList(values).get(1),Arrays.asList(values).get(0));
				}
			} catch (IOException e) {
				// TODO Bloc catch généré automatiquement
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Bloc catch généré automatiquement
			e.printStackTrace();
		}
		return dic;
	}
	public static ArrayList<String> getALignment(String sourcekb,Model targetKB,String functionUri,String matchcond){
		long endTime,duration,startTime;
		startTime = System.nanoTime();
		ArrayList<String> list = new ArrayList<String>();
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(sourcekb));
			String line,source,tsource;
			int c=0;
			try {
				while ((line = br.readLine()) != null) {
					String[] values = line.split("\t");
					source=values[0];
					tsource=values[1];
					String target=findCandidates(tsource,targetKB,functionUri,matchcond);
					if(target!=null) {
						list.add(source+"\t"+target);
					}
					c++;
					if(c%1000==0) {
						//endTime = System.nanoTime();
						//duration = (endTime - startTime);
						//System.out.println(duration/1000000000+" s");
						System.out.println("after "+c+" found ="+list.size());
						//break;
					}
				}
			} catch (IOException e) {
				// TODO Bloc catch généré automatiquement
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Bloc catch généré automatiquement
			e.printStackTrace();
		}
		endTime = System.nanoTime();
		duration = (endTime - startTime);
		System.out.println(duration/1000000000.0+" s");
		return list;
	}

	public static void makePrior(String kbref,String matcher,ArrayList<String> found) {
		Dictionary<String,String> dic1 = new Hashtable<String,String>();
		Dictionary<String,String> dic2 = new Hashtable<String,String>();
		dic1=makeDic(kbref+"0_3/ent_ids_1");
		dic2=makeDic(kbref+"0_3/ent_ids_2");
		
		String s,t;
		ArrayList<String> sup=new ArrayList<>();
		for (String fo:found) 
		{ 
			String[] values = fo.split("\t");
			s=dic1.get(values[0]);
			t=dic2.get(values[1]);
			if(s!=null && t!=null)sup.add(s+"\t"+t+System.lineSeparator());

		}
		System.out.println("sup size : "+sup.size());
		
	    File directory = new File(kbref+matcher);
	    if (! directory.exists()){
	        directory.mkdir();
	    }
		try {
			FileWriter writer = new FileWriter(kbref+matcher+"/sup_pairs2");
			for(String str: sup) {
				writer.write(str);
			}
			writer.close();
		} catch (IOException e) {
			// TODO Bloc catch généré automatiquement
			e.printStackTrace();
		}
	}
	public static void processKB(String kbref,String matcher,String matchcond) {
		String kbname="t_triples";
		String fullname=kbref+kbname+".ttl";
		Model model=RDFDataMgr.loadModel(fullname);
		matcherName=matcher;
		String functionUri = "http://www.example.org/"+matcher;
		if(matcher.equals("SubString"))FunctionRegistry.get().put(functionUri ,SubStringFilter.class );
		else if(matcher.equals("Levenshtein"))FunctionRegistry.get().put(functionUri ,LevenshteinFilter.class );
		else if(matcher.equals("Fuzzy"))FunctionRegistry.get().put(functionUri ,FuzzyFilter.class );
		else if(matcher.equals("JaroWinkler"))FunctionRegistry.get().put(functionUri ,JaroWinklerFilter.class );
		else if(matcher.equals("DSC"))FunctionRegistry.get().put(functionUri ,DSCFilter.class );
		else if(matcher.equals("W2V"))FunctionRegistry.get().put(functionUri ,W2vFilter.class );
		else {
			System.out.println("No matcher found");
			System.exit(1);
		}
		ArrayList<String>found=getALignment(kbref+"0_3/translated",model,functionUri,matchcond);
		System.out.println(found.size());
		makePrior(kbref,matcher,found);
	}
	
	
	public static void main(String[] args) throws IOException {
		RIOT.init() ;
		
		Options options = new Options();
		Option input = new Option("i", "input", true, "path to kb");
		Option matcher = new Option("m", "matcher", true, "string similarity metric");
		Option cond = new Option("c", "cond", true, "filtering condition");
		Option w2v = new Option("p", "path", true, "word2vec path");
		cond.setRequired(true);
		input.setRequired(true);
		matcher.setRequired(true);
		w2v.setRequired(true);
		options.addOption(input);
		options.addOption(matcher);
		options.addOption(cond);
		options.addOption(w2v);
		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd = null;
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("utility-name", options);

			System.exit(1);
		}
		String inputFilePath = cmd.getOptionValue("input");
		String matchername = cmd.getOptionValue("matcher");
		String matchcond=cmd.getOptionValue("cond");
		if(matchername.contains("W2V")) {
			wordVectors=Word2VecModel.fromBinFile(new File(cmd.getOptionValue("path"))).forSearch();
		}
		System.out.println("Matcher --> "+matchername);
		System.out.println("Matcher condition --> "+matchcond);
//		String inputFilePath="/nfs/nas4/cicoda_tmp/cicoda_tmp/Alignement/dbp15k/fr_en/";
//		String matchername = "Levenshtein";
//		String matchcond="<2";
		processKB(inputFilePath,matchername,matchcond);
	}

}