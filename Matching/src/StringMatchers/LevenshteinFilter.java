package StringMatchers;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.sparql.function.FunctionBase2;

public class LevenshteinFilter extends FunctionBase2 
{  
     public NodeValue exec(NodeValue value1, NodeValue value2){
         int i = StringUtils.getLevenshteinDistance(value1.asString(), value2.asString()); 
         return NodeValue.makeInteger(i); 
     }
}