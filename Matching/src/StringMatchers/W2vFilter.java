package StringMatchers;

import org.allenai.word2vec.Searcher.UnknownWordException;
import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.sparql.function.FunctionBase2;

public class W2vFilter extends FunctionBase2 {

	public NodeValue exec(NodeValue value1, NodeValue value2){
		double i;
		try {
			String[] values1 = value1.asString().split("/");
			String[] values2 = value2.asString().split("/");
			i = StringMatcher.wordVectors.cosineDistance(values1[values1.length-1].toLowerCase(),values2[values2.length-1].toLowerCase());
			return NodeValue.makeDouble(i); 
		} catch (UnknownWordException e) {
			// TODO Bloc catch généré automatiquement
			return NodeValue.makeDouble(0);
		}
        
    }

}
