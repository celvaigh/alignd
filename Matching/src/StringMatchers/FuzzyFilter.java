package StringMatchers;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.sparql.function.FunctionBase2;

public class FuzzyFilter extends FunctionBase2 {

	public NodeValue exec(NodeValue value1, NodeValue value2){
        double i = StringUtils.getFuzzyDistance(value1.asString(), value2.asString(),Locale.ENGLISH); 
        return NodeValue.makeDouble(i); 
    }

}
