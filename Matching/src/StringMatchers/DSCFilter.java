package StringMatchers;
import java.util.HashSet;
import java.util.Set;

import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.sparql.function.FunctionBase2;

public class DSCFilter extends FunctionBase2 {

	@Override
	public NodeValue exec(NodeValue value1, NodeValue value2) {
		double i = DSC(value1.asString(), value2.asString()); 
        return NodeValue.makeDouble(i); 
	}
	
	public static double DSC(String s1, String s2)
	{
		Set<String> nx = new HashSet<String>();
		Set<String> ny = new HashSet<String>();
		for (int i=0; i < s1.length()-1; i++) {
			char x1 = s1.charAt(i);
			char x2 = s1.charAt(i+1);
			String tmp = "" + x1 + x2;
			nx.add(tmp);
		}
		for (int j=0; j < s2.length()-1; j++) {
			char y1 = s2.charAt(j);
			char y2 = s2.charAt(j+1);
			String tmp = "" + y1 + y2;
			ny.add(tmp);
		}

		Set<String> intersection = new HashSet<String>(nx);
		intersection.retainAll(ny);
		double totcombigrams = intersection.size();

		return (2*totcombigrams) / (nx.size()+ny.size());
	}

}
