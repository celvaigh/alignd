package StringMatchers;
import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.sparql.function.FunctionBase2;

public class SubStringFilter extends FunctionBase2 {

	public NodeValue exec(NodeValue value1, NodeValue value2) {
		boolean i = value1.asString().contains(value2.asString()) || value2.asString().contains(value1.asString());
		// System.out.println("FuzzyFilter(" +value1+" ,"+value2+") = "+i);
		// System.exit(0);
		return NodeValue.makeBoolean(i);
	}

}
