package StringMatchers;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.sparql.function.FunctionBase2;

public class JaroWinklerFilter extends FunctionBase2 {
	 public NodeValue exec(NodeValue value1, NodeValue value2){
         double i = StringUtils.getJaroWinklerDistance(value1.asString(), value2.asString()); 
         return NodeValue.makeDouble(i); 
     }
}
