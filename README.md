# AlignD
Source code and datasets for ICTAI-2020 paper "_[A correlation-based entity embedding approach for robust entity linking]()_".

## Dataset
We use two datasets, namely DBP15K and DWY100K. DBP15K can be found [here](https://github.com/nju-websoft/JAPE).


## Code
Folder "code" contains all codes of AlignD, in which:
* "AlignD.py" is the implementation of AlignD;
* "param.py" is the config file.

Folder "Matching" contains all codes for string matching, in which:
* "StringMatcher.java" is the main file for building a seed from scratch.
* "{name}Filter.java" is a string matching algorithm based on the filter {name}, where {name} is in {DSC, Fuzzy, JaroWinkler,   Levenshtein, SubString, W2V}.
* "StringMatcher.jar" is the associated java liberary



### Dependencies
* Python 3
* torch
* Scipy
* Numpy
* Graph-tool or igraph or NetworkX
* Java
* Jena
* log4j


## Citation
If you use this model or code, please cite it as follows:      
```
@inproceedings{AlignD,
  author    = {Cheikh Brahim El Vaigh and François Torregrossa and Robin Allesiardo and Guillaume Gravier and Pascale Sébillot},
  title     = {A correlation-based entity embedding approach for robust entity linking},
  booktitle = {ICTAI},
  pages     = {--},
  year      = {2020}
}
```

